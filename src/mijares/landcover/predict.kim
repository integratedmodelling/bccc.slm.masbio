private namespace mijares.landcover.predict;

observe earth:Region named mijares
	over space(
		urn = "local:joaopompeu:mijares.local:MIJARES_4326",
		grid = "30 m",
		projection = "EPSG:4326"),
		time(start = 2020, end = 2030, step = 1.year)
		;
		
private model "local:joaopompeu:mijares.local:LUC_2018_pheno_clip"
	as landcover:LandCoverType
		classified into
			landcover:ArtificialSurface 				if 0,
			landcover:NonIrrigatedArableLand 			if 1,
			landcover:PermanentlyIrrigatedArableLand 	if 2,
			landcover:FruitAndBerryPlantation 			if 3,
			landcover:OliveGrove 						if 4,
			landcover:BroadleafForest 					if 5,
			landcover:ConiferousForest 					if 6,
			landcover:MixedForest 						if 7,
			landcover:Grassland 						if 8,
			landcover:SclerophyllousVegetation 			if 9,
			landcover:TransitionalWoodlandScrub 		if 10,
			landcover:BeachDuneAndSand 					if 11,
			landcover:WaterBody 						if 12
		//over space(urn='local:joaopompeu:mijares.local:MIJARES_4326')
;
	

// LAND COVER CHANGE MODELLING //

//  predict changes 


@time(step=1.year)
model change in landcover:LandCoverType
    observing
		//@predictor distance to infrastructure:Highway,
		@predictor distance to earth:Waterway,
		//@predictor distance to earth:Coastline,
		//@predictor distance to conservation:ProtectedArea,
		@predictor geography:Slope,
		@predictor geography:Elevation,
		@predictor geography:Aspect//,@predictor soil.incubation:SoilDepth
	using klab.landcover.allocate(
		suitability = 'local:joaopompeu:mijares.local:suitability.bn.mijares',
        greedy = false,
       	resistances = {
			landcover:ArtificialSurface:				1,
			landcover:NonIrrigatedArableLand:			0.93,
			landcover:PermanentlyIrrigatedArableLand: 	0.93,
			landcover:FruitAndBerryPlantation:			0.93,
			landcover:OliveGrove:						0.93,
			landcover:BroadleafForest:  	  			0.93,
			landcover:ConiferousForest: 	  			0.55, 
			landcover:MixedForest:        	  			0.93,
			landcover:Grassland:         	  			0.93,
			landcover:SclerophyllousVegetation: 		0.93,
			landcover:TransitionalWoodlandScrub: 		0.93,
			landcover:BeachDuneAndSand:      			0.93,
			landcover:WaterBody: 						1		
	      	},
		demand = {
			landcover:BroadleafForest:   				80.km^2,
			landcover:SclerophyllousVegetation:			10.km^2,
			landcover:MixedForest:						20.km^2
		},
		deviations = {
			landcover:BroadleafForest:   				8.km^2,
			landcover:SclerophyllousVegetation:			1.km^2,
			landcover:MixedForest:						2.km^2
		},
		transitions = {{
			
						*								|landcover:ArtificialSurface	|landcover:NonIrrigatedArableLand	|landcover:PermanentlyIrrigatedArableLand	|landcover:FruitAndBerryPlantation	|landcover:OliveGrove	|landcover:BroadleafForest	|landcover:ConiferousForest	|landcover:MixedForest	|landcover:Grassland	|landcover:SclerophyllousVegetation	|landcover:TransitionalWoodlandScrub	|landcover:BeachDuneAndSand	|landcover:WaterBody				,						
			landcover:ArtificialSurface					|			0					|			0.1						|0.1										|			0.1						|	0.1					|		0.2					|		0					|	0					|	0					|			0						|				0						|		0					|	0					,						
			landcover:NonIrrigatedArableLand			|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.2					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:PermanentlyIrrigatedArableLand	|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.2					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:FruitAndBerryPlantation			|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.2					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,	
			landcover:OliveGrove						|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.2					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:BroadleafForest					|			0					|			0						|0											|			0						|	0					|		0					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:ConiferousForest					|			0.1					|			0						|0											|			0						|	0					|		0.5					|		0					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:MixedForest						|			0					|			0						|0											|			0						|	0					|		0.1					|		0.2					|	0					|	0					|			0.1						|				0						|		0					|	0					,	
			landcover:Grassland							|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.5					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:SclerophyllousVegetation			|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.2					|		0.2					|	0.2					|	0					|			0						|				0						|		0					|	0					,		
			landcover:TransitionalWoodlandScrub			|			0.1					|			0.1						|0.1										|			0.1						|	0.1					|		0.5					|		0.2					|	0.2					|	0					|			0.1						|				0						|		0					|	0					,
			landcover:BeachDuneAndSand					|			0					|			0						|0											|			0						|	0					|		0					|		0					|	0					|	0					|			0						|				0						|		0					|	0					,
			landcover:WaterBody							|			0					|			0						|0											|			0						|	0					|		0					|		0					|	0					|	0					|			0						|				0						|		0					|	0					
	
		}},
		seed = 1
    );

